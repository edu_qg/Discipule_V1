class CreateCadastros < ActiveRecord::Migration
  def change
    create_table :cadastros do |t|
      t.string :Nome
      t.string :Senha
      t.string :Funcao

      t.timestamps null: false
    end
  end
end
