class CreateCadastroUsuarios < ActiveRecord::Migration
  def change
    create_table :cadastro_usuarios do |t|
      t.string :nome
      t.string :senha
      t.string :funcao

      t.timestamps null: false
    end
  end
end
